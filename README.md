# Connect to ssh server using Vk-API
This module allows to use ssh-server by VK-API.

# Authorization
In order to log in you must add your vk-token and data ssh-server.

`token=`

###### `host = ''`
###### `login = ''`
###### `password = ''`


# Dependencies

##### You must download a few libs:
`pip3 install paramiko`

`pip3 install vk`

`pip3 install requests`

# Using

In order to send command to ssh-server, you must send command to vk-bot. 
Bot will send answer from linux-server.