import paramiko
import requests
import vk

host = ''  # host
login = ''  # login
password = ''  # password
token = ''
group_id = 1  # int group id

session = (vk.Session(access_token=token))
api = vk.API(session)


def commands(comm):
    try:
        """Send command to server"""
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, username=login, password=password)
        stdin, stdout, stderr = ssh.exec_command(comm)
        stdout = stdout.read().decode('utf-8')
        if stdout != '':
            return stdout
        else:
            return "Сервер не вернул ответа"
    except Exception:
        return "Error"


z = api.groups.getLongPollServer(group_id=group_id, v=5.8)
key = z.get('key')
server = z.get('server')
ts = z.get('ts')

url = '{}?act=a_check&key={}&ts={}&wait=25'.format(server, key, ts)

r = requests.get(url)
r = r.json()

while True:

    url = '{}?act=a_check&key={}&ts={}&wait=25'.format(server, key, ts)

    r = requests.get(url)
    r = r.json()
    if r.get('failed') is not None:
        key = api.groups.getLongPollServer(group_id=group_id, v=5.8)['key']

    if ts != r.get('ts') and r.get('updates'):
        body = r.get('updates')[0]['object']['text']
        user_id = r.get('updates')[0]['object']['peer_id']

        api.messages.send(v=5.8, user_id=user_id, message=commands(body))
    ts = r.get('ts')
